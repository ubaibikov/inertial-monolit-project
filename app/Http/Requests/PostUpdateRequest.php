<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostUpdateRequest extends FormRequest
{
    protected $errorBag = 'postUpdate';

    // You can tap in here if you need really dynamic names
    public function prepareForValidation()
    {
        $this->errorBag = "post.{$this->route('posts.update')}";
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'post.title'  => 'required',
            'post.author' => 'required',
            'post.body' => 'required',
        ];
    }
}
