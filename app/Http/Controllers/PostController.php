<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostCreate;
use App\Http\Requests\PostCreateRequest;
use App\Http\Requests\PostUpdate;
use App\Http\Requests\PostUpdateRequest;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Inertia\Inertia;

class PostController extends Controller
{
    /**
     * @property Model $post Post Model
     */
    protected $post;

    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /**
         * method render() first Rendering your component
         */
        return Inertia::render('Posts/Index', [
            'posts' => $this->post::all()->map(function ($post) {
                return [
                    'id'         => $post->id,
                    'title'      => $post->title,
                    'author'     => $post->author,
                    'body'       => $post->body,
                    'private'    => $post->private,
                    'edit_url'   => URL::route('posts.edit', $post),
                ];
            }),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Inertia::render('Posts/Create', ['store_url' => URL::route('posts.store')]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostCreateRequest $request)
    {
        $postCreate = $this->post::create($request->post);

        return Redirect::route('posts.index')->with('status', 'Post Created');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
        /**
         * method render() first Rendering your component
         */
        return Inertia::render('Posts/Edit', [
            'post' => $this->post::find($id)->toArray(),
            'update_url' => URL::route('posts.update', $id),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id Post identifier
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(int $id, PostUpdateRequest $request)
    {
        $postUpdate = $this->post::find($id)->update($request->post);

        return Redirect::route('posts.index')->with('status', 'Post Updated');
    }

    /**
     * Delete the specified resource in storage.
     *
     * @param int $id Post identifier
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $postDelete = $this->post::find($id)->delete();

        return Redirect::route('posts.index')->with('status', 'Post Deleted');
    }
}
