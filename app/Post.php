<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{

    /**
     * @property array $fillable Fillable model rws
     */
    protected $fillable = [
        'title', 'author', 'body','private',
    ];
}
