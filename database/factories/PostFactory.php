<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Post;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {
    return [
        'author' => $faker->name,
        'title' => $faker->sentence(4),
        'body' => $faker->text,
        'private' => rand(0, 1),
    ];
});
