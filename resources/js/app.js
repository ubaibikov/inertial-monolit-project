require('./bootstrap');

import { InertiaApp } from '@inertiajs/inertia-vue'
import {Ziggy} from './../assets/js/ziggy'

import Vue from 'vue'

Vue.use(InertiaApp)

const app = document.getElementById('app')


Vue.prototype.$route = (...args) => route(...args).url()
Vue.mixin({
    methods: {
      error(field, errorBag = 'default') {
        if (!this.$page.errors.hasOwnProperty(errorBag)) {
          return null;
        }

        if (this.$page.errors[errorBag].hasOwnProperty(field)) {
          return this.$page.errors[errorBag][field][0];
        }

        return null;
      }
    }
  });
new Vue({
  render: h => h(InertiaApp, {
    props: {
      initialPage: JSON.parse(app.dataset.page),
      resolveComponent: name => import(`./Pages/${name}`).then(module => module.default),
    },
  }),
}).$mount(app)

